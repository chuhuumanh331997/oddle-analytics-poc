cube(`AnalyticsCubePoc`, {
  sql: `SELECT * FROM oddle_analytics_cube_js.analytics_cube_poc`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, menuId, menuName, deliveryDate]
    }
  },
  
  dimensions: {
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    menuId: {
      sql: `menu_id`,
      type: `string`
    },
    
    menuName: {
      sql: `menu_name`,
      type: `string`
    },
    
    totalAmount: {
      sql: `total_amount`,
      type: `string`
    },
    
    submittedOn: {
      sql: `submitted_on`,
      type: `time`
    },
    
    deliveryDate: {
      sql: `delivery_date`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
