import { merge } from 'lodash'
import ReactApexChart from 'react-apexcharts'
//
import BaseOptionChart from './BaseOptionChart'

// ----------------------------------------------------------------------

export default function ChartColumnStacked({
  series,
  xaxis,
  type,
}: {
  series: any[]
  xaxis: any[]
  type: string
}) {
  const chartOptions = merge(BaseOptionChart(), {
    chart: {
      stacked: true,
      zoom: { enabled: true },
      toolbar: {
        show: true,
      },
    },
    legend: { itemMargin: { vertical: 8 }, position: 'right', offsetY: 20 },
    plotOptions: { bar: { columnWidth: '14%', borderRadius: 4 } },
    title: {
      text: type,
      align: 'left',
      margin: 10,
      offsetX: 0,
      offsetY: 0,
      floating: false,
      style: {
        fontSize: '14px',
        fontWeight: '600',
        fontFamily: 'Be Vietnam',
        color: '#263238',
      },
    },
    stroke: { show: false },
    xaxis,
  })
  return (
    <ReactApexChart
      type="bar"
      series={series}
      options={chartOptions}
      height={635}
    />
  )
}
