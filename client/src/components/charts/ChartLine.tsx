import { merge } from 'lodash'
import ReactApexChart from 'react-apexcharts'
//
import BaseOptionChart from './BaseOptionChart'

// ----------------------------------------------------------------------

export default function ChartLine(props) {
  const { series = [], options = {} } = props
  const chartOptions = merge(BaseOptionChart(), options)

  return (
    <ReactApexChart
      type="line"
      series={series}
      options={chartOptions}
      height={320}
    />
  )
}
