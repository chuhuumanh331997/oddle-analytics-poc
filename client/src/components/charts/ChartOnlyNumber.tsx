import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { Grid } from '@material-ui/core'
import { LocalAtm } from '@material-ui/icons'

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
  },
  iconDola: {
    margin: 'auto auto 24px',
    display: 'flex',
    borderRadius: '50%',
    alignItems: 'center',
    width: '64px',
    height: '64px',
    justifyContent: 'center',
    // color:' rgb(0, 123, 85)',
    backgroundImage: `linear-gradient( 
        135deg
         , rgba(0, 123, 85, 0) 0%, rgba(0, 123, 85, 0.24) 100%);`,
  },
}))

function ChartOnlyNumber(props) {
  const { title, numberOrder } = props
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Grid>
        <div className={classes.iconDola}>
          <LocalAtm />
        </div>
      </Grid>
      <div>
        <h1>{numberOrder}</h1>
        <h4>{title}</h4>
      </div>
    </div>
  )
}

export default ChartOnlyNumber
