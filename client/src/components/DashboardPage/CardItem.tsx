import { Card, CardContent } from '@material-ui/core'

const CardItem = ({ children }) => (
  <Card>
    <CardContent>{children}</CardContent>
  </Card>
)
export default CardItem
