import React, { useState } from 'react'
import FormControl from '@material-ui/core/FormControl'
import _ from 'lodash'
import moment from 'moment'
import FilterListIcon from '@material-ui/icons/FilterList'
import Drawer from '@material-ui/core/Drawer'
import CloseIcon from '@material-ui/icons/Close'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import TextField from '@material-ui/core/TextField'
import DateRangePicker from '@material-ui/lab/DateRangePicker'
import AdapterDateFns from '@material-ui/lab/AdapterDateFns'
import LocalizationProvider from '@material-ui/lab/LocalizationProvider'
import Box from '@material-ui/core/Box'

function FilterDashboard(props) {
  const {
    statusOrder,
    rangeStart,
    setRangeStart,
    rangeEnd,
    setRangeEnd,
    setStatusOrder,
    menu,
  } = props
  const [drawer, setDrawer] = useState(false)
  const getStatusSelected = () => {
    const statusDetail = _.find(statusOrder, (item) => item.selected)
    return statusDetail ? statusDetail.value : ''
  }

  const getLabelStatus = () => {
    const statusDetail = _.find(statusOrder, (item) => item.selected)
    return statusDetail ? statusDetail.label : ''
  }

  const toggleDrawer = () => {
    setDrawer(!drawer)
  }

  const changeStatusOrder = (e) => {
    const { value } = e.target
    const orderStatusNew = _.map(statusOrder, (item) => {
      let selected = false
      if (item.value === value) {
        selected = true
      }
      // eslint-disable-next-line no-param-reassign
      item.selected = selected
      return item
    })
    setStatusOrder(orderStatusNew)
  }
  return (
    <>
      <div className="filter-analytics">
        <div
          className="filter-analytics-item filter-analytics-item__left"
          onClick={toggleDrawer}
          aria-hidden="true"
        >
          <div className="analytics-item--filter-selected">
            <span>
              <b>Menu Name Single </b>
              {menu['AnalyticsCubePoc.menuName']}
            </span>
          </div>
          <div className="analytics-item--filter-selected">
            <span>
              <b>Date Type </b>
              {getLabelStatus()}
            </span>
          </div>
          <div className="analytics-item--filter-selected">
            <span>
              <b>Date Range </b>
              {moment(rangeStart).format('MM/DD/YYYY')} -{' '}
              {moment(rangeEnd).format('MM/DD/YYYY')}
            </span>
          </div>
        </div>
        <div
          className="filter-analytics-item filter-analytics-item__right"
          aria-hidden="true"
          onClick={toggleDrawer}
        >
          <div className="filter-analytics-item__icon">
            <FilterListIcon />
          </div>
        </div>
      </div>
      <Drawer anchor="right" open={drawer} onClose={toggleDrawer}>
        <div className="draw-right-filter">
          <div className="draw-right-filter__header">
            <h3>Filters</h3>
            <CloseIcon onClick={toggleDrawer} />
          </div>
          <hr className="hr-drawer" />
          <div className="draw-right-filter__content">
            <div className="draw-right-filter__content--item">
              <FormControl component="fieldset">
                <h3>Order Status</h3>
                <RadioGroup
                  aria-label="status"
                  name="status"
                  value={getStatusSelected()}
                  onChange={changeStatusOrder}
                >
                  {_.map(statusOrder, (status) => (
                    <FormControlLabel
                      value={status.value}
                      control={<Radio />}
                      label={status.label}
                    />
                  ))}
                </RadioGroup>
              </FormControl>
            </div>
            <div className="draw-right-filter__content--item">
              <FormControl component="fieldset">
                <h3>Date Range</h3>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DateRangePicker
                    startText="Range-start"
                    endText="RangeEnd"
                    value={[
                      moment(rangeStart).format('MM/DD/YYYY'),
                      moment(rangeEnd).format('MM/DD/YYYY'),
                    ]}
                    onChange={(newValue) => {
                      if (newValue[0] && newValue[1]) {
                        setRangeStart(moment(newValue[0]))
                        setRangeEnd(moment(newValue[1]))
                      }
                    }}
                    renderInput={(startProps, endProps) => (
                      <>
                        <TextField {...startProps} variant="standard" />
                        <Box sx={{ mx: 2 }}> to </Box>
                        <TextField {...endProps} variant="standard" />
                      </>
                    )}
                  />
                </LocalizationProvider>
              </FormControl>
            </div>
          </div>
        </div>
      </Drawer>
    </>
  )
}

export default FilterDashboard
