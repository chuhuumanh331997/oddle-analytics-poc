export function queryCheckMenuById(menuId) {
  return {
    measures: [],
    timeDimensions: [],
    order: {
      'AnalyticsCubePoc.menuId': 'asc',
    },
    filters: [
      {
        member: 'AnalyticsCubePoc.menuId',
        operator: 'equals',
        values: [menuId],
      },
    ],
    dimensions: ['AnalyticsCubePoc.menuId', 'AnalyticsCubePoc.menuName'],
  }
}

export function queryGetAllOrder(menuId, status, range) {
  return {
    measures: ['AnalyticsCubePoc.count'],
    timeDimensions: [
      {
        dimension: status,
        granularity: 'day',
        dateRange: range,
      },
    ],
    filters: [
      {
        member: 'AnalyticsCubePoc.menuId',
        operator: 'equals',
        values: [menuId],
      },
    ],
    dimensions: ['AnalyticsCubePoc.totalAmount'],
  }
}

export function queryStoreData() {
  return {
    type: 'Sales by Stores by Selected Dates',
    columns: [
      { field: 'store_name', headerName: 'Store Name', width: 300 },
      {
        field: 'number_of_orders',
        headerName: 'Number of Orders',
        width: 190,
      },
      {
        field: 'total_sales',
        headerName: 'Total Sales ',
        width: 180,
      },
    ],
    rows: [],
  }
}

export function queryTopSale() {
  return {
    type: 'Top 15 Products by Units Sold',
    columns: [
      { field: 'name', headerName: 'Item Name', width: 300 },
      {
        field: 'total_quantity',
        headerName: 'Total Quantity Ordered',
        width: 190,
      },
      {
        field: 'total_sales_from_items',
        headerName: 'Total Sales from Items',
        width: 180,
      },
    ],
    rows: [],
  }
}

export function queryGetAllMenu() {
  return {
    dimensions: ['AnalyticsCubePoc.menuName', 'AnalyticsCubePoc.menuId'],
    timeDimensions: [],
    order: {
      'AnalyticsCubePoc.menuId': 'asc',
    },
  }
}
