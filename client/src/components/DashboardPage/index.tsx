import React, { useEffect, useState } from 'react'
import { Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import moment from 'moment'
import _ from 'lodash'
import { useCubeQuery } from '@cubejs-client/react'
import { useRouter } from 'next/router'
import SalesOverTimeLine from './charts/SalesOverTimeLine'
import AverageOrderValueTimeLine from './charts/AverageOrderValueTimeLine'
import TotalOrderValueTimeLine from './charts/TotalOrderValueTimeLine'
import TotalSales from './charts/TotalSales'
import AverageOrderValue from './charts/AverageOrderValue'
import TotalOrders from './charts/TotalOrders'
import LoadingPage from '../../utils/common/LoadingPage'
import SalesBySomeThing from './charts/SalesBySomeThing'
import TableWithData from './charts/TableWithData'
import {
  queryCheckMenuById,
  queryGetAllOrder,
  queryStoreData,
  queryTopSale,
} from './query'
import FilterDashboard from './FilterDashboard'
import CardItem from './CardItem'

const styles = makeStyles(() => ({
  dashboard: {
    padding: '36px',
  },
  layoutItem: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '20px',
  },
}))

const getSelectStatusOrder = (statusOrders) => {
  const statusDetail = _.find(statusOrders, (item) => item.selected)
  return statusDetail || ''
}

const convertDateRange = (rangeStart, rangeEnd) => ({
  start: moment(rangeStart).format('YYYY/MM/DD'),
  end: moment(rangeEnd).format('YYYY/MM/DD'),
})
const generateQueryParams = (obj) => {
  let queryString = ''
  Object.keys(obj).forEach((key) => {
    const specialCharacters = queryString.indexOf('?') !== -1 ? '&' : '?'
    queryString += `${specialCharacters}${key}=${obj[key]}`
  })
  return queryString
}
const Dashboard = () => {
  const router = useRouter()
  const [menuId, setMenuId] = useState('')
  const [rangeStart, setRangeStart] = useState('')
  const [rangeEnd, setRangeEnd] = useState('')
  const [statusOrder, setStatusOrder] = useState([
    {
      value: 'AnalyticsCubePoc.submittedOn',
      label: 'Submitted On',
      selected: true,
    },
    {
      value: 'AnalyticsCubePoc.deliveryDate',
      label: 'Delivery Date',
      selected: false,
    },
  ])
  const queryCheckMenuId: any = queryCheckMenuById(menuId)
  const { resultSet } = useCubeQuery(queryCheckMenuId)

  const statusOrderSelected = getSelectStatusOrder(statusOrder)
  const range = convertDateRange(rangeStart, rangeEnd)
  const queryGetAllOrderByStatus = queryGetAllOrder(
    menuId,
    statusOrderSelected.value,
    [range.start, range.end]
  )
  const classes = styles()

  const salebyStoreData = queryStoreData()
  const queryGetSalesByOrderValue = queryGetAllOrder(
    menuId,
    statusOrderSelected.value,
    [range.start, range.end]
  )

  const queryGetSalesByOrderType = queryGetSalesByOrderValue
  const topSaleData = queryTopSale()

  useEffect(() => {
    const routerQuery: any = router.query
    let defaultRangeStart: any = moment().subtract('7', 'days')
    let defaultRangeEnd: any = moment()
    let defaultStatus = 'AnalyticsCubePoc.submittedOn'
    if (routerQuery.rangeStart) {
      defaultRangeStart = moment(routerQuery.rangeStart)
    }
    if (routerQuery.rangeEnd) {
      defaultRangeEnd = moment(routerQuery.rangeEnd)
    }
    if (routerQuery.status) {
      defaultStatus = routerQuery.status
    }
    const newStatusOrder: any = statusOrder.map((statusOld) => {
      const newStatus: any = { ...statusOld }
      newStatus.selected = newStatus.value === defaultStatus
      return newStatus
    })
    setMenuId(routerQuery.menuId)
    setStatusOrder(newStatusOrder)
    setRangeStart(defaultRangeStart)
    setRangeEnd(defaultRangeEnd)

    const queryString = generateQueryParams({
      menuId : routerQuery.menuId,
      rangeStart: defaultRangeStart.format('MM/DD/YYYY'),
      rangeEnd: defaultRangeEnd.format('MM/DD/YYYY'),
      status: defaultStatus,
    })
    router.push({
      search: queryString,
    })
  }, [])

  useEffect(() => {
    const objStatus = getSelectStatusOrder(statusOrder)
    const queryString = generateQueryParams({
      menuId,
      rangeStart: moment(rangeStart).format('MM/DD/YYYY'),
      rangeEnd: moment(rangeEnd).format('MM/DD/YYYY'),
      status: objStatus.value,
    })
    router.push({
      search: queryString,
    })
  }, [rangeStart, rangeEnd, statusOrder])

  if (resultSet) {
    const rs: any = resultSet
    if (!_.isEmpty(rs.loadResponse.data)) {
      return (
        <div className={classes.dashboard}>
          <div className="dashboard-filter">
            <FilterDashboard
              menu={rs.loadResponse.data[0]}
              rangeStart={rangeStart}
              rangeEnd={rangeEnd}
              setRangeStart={setRangeStart}
              setRangeEnd={setRangeEnd}
              setStatusOrder={setStatusOrder}
              statusOrder={statusOrder}
            />
          </div>
          <div className="dashboard-content">
            <div className={classes.layoutItem}>
              <Grid container spacing={2}>
                <Grid item lg={4} sm={6} xl={4} xs={12}>
                  <TotalSales query={queryGetAllOrderByStatus} />
                </Grid>
                <Grid item lg={4} sm={6} xl={4} xs={12}>
                  <AverageOrderValue query={queryGetAllOrderByStatus} />
                </Grid>
                <Grid item lg={4} sm={6} xl={4} xs={12}>
                  <TotalOrders query={queryGetAllOrderByStatus} />
                </Grid>
              </Grid>
            </div>
            <div className={classes.layoutItem}>
              <Grid container spacing={2}>
                <Grid item lg={4} sm={6} xl={4} xs={12}>
                  <CardItem>
                    <SalesOverTimeLine query={queryGetAllOrderByStatus} />
                  </CardItem>
                </Grid>
                <Grid item lg={4} sm={6} xl={4} xs={12}>
                  <CardItem>
                    <AverageOrderValueTimeLine
                      query={queryGetAllOrderByStatus}
                    />
                  </CardItem>
                </Grid>
                <Grid item lg={4} sm={6} xl={4} xs={12}>
                  <CardItem>
                    <TotalOrderValueTimeLine query={queryGetAllOrderByStatus} />
                  </CardItem>
                </Grid>
              </Grid>
            </div>
            <div className={classes.layoutItem}>
              <Grid container spacing={2}>
                <Grid item lg={6} sm={12} xl={6} xs={12}>
                  <CardItem>
                    <TableWithData {...topSaleData} />
                  </CardItem>
                </Grid>
                <Grid item lg={6} sm={12} xl={6} xs={12}>
                  <CardItem>
                    <SalesBySomeThing
                      query={queryGetSalesByOrderValue}
                      type="Sales by Order Value"
                    />
                  </CardItem>
                </Grid>
              </Grid>
            </div>
            <div className={classes.layoutItem}>
              <Grid container spacing={2}>
                <Grid item lg={6} sm={12} xl={6} xs={12}>
                  <CardItem>
                    <TableWithData {...salebyStoreData} />
                  </CardItem>
                </Grid>
                <Grid item lg={6} sm={12} xl={6} xs={12}>
                  <CardItem>
                    <SalesBySomeThing
                      query={queryGetSalesByOrderType}
                      type="Sales by Order Type"
                    />
                  </CardItem>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      )
    }
    return (
      <div className="error-page">
        <div className="error-page__content">
          <h1 className="title">Oops! (#422)</h1>
          <p className="description">No shareable link found</p>
        </div>
      </div>
    )
  }

  return <LoadingPage />
}

export default Dashboard
