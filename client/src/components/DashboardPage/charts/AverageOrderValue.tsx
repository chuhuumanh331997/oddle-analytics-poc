import { useCubeQuery } from '@cubejs-client/react'
import {
  Card,
  CardContent,
  CircularProgress,
  makeStyles,
} from '@material-ui/core'
import React from 'react'
import ChartOnlyNumber from '../../charts/ChartOnlyNumber'
import { nFormatter } from '../../../utils/nFomatter'

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'rgb(4, 41, 122)',
    backgroundColor: 'rgb(208, 242, 255)',
  },
}))

const handleDataCube = (dataSet) => {
  let sum: any = 0
  let numberOrders = 0
  dataSet.forEach((data) => {
    const count = Number(data['AnalyticsCubePoc.count'])
    const totalAmount = Number(data['AnalyticsCubePoc.totalAmount'])
    numberOrders += count
    sum += count * totalAmount
  })
  const rs: any = nFormatter(sum / numberOrders, 0)
  return `$${rs[0]}${rs[1]}`
}

function AverageOrderValue(props) {
  const { query } = props
  const { resultSet }: any = useCubeQuery(query)

  const classes = useStyles()
  if (resultSet) {
    const numberOrder = handleDataCube(resultSet.loadResponse.data)

    return (
      <div>
        <Card className={classes.root}>
          <CardContent>
            <ChartOnlyNumber
              numberOrder={numberOrder}
              title="Average Order Value"
            />
          </CardContent>
        </Card>
      </div>
    )
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '300px',
      }}
    >
      <CircularProgress color="secondary" />
    </div>
  )
}

export default AverageOrderValue
