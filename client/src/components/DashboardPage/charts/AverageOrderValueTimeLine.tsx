import React from 'react'
import { useCubeQuery } from '@cubejs-client/react'
import {
  CircularProgress,
  makeStyles,
  Grid,
  Typography,
} from '@material-ui/core'
import moment from 'moment'
import _ from 'lodash'
import { ChartLine } from '../../charts'

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: 500,
  },
}))

const handleDataCube = (data, query) => {
  const categories = []
  const series = []
  const totalMoneyOrder = _.reduce(
    data,
    (sum, item) => sum + item[query.dimensions[0]] * item[query.measures[0]],
    0
  )
  const result = _.groupBy(data, query.timeDimensions[0].dimension)
  Object.keys(result).forEach((key) => {
    const dateNow = moment(key).format('DD/MM/YYYY')
    categories.push(dateNow)
    const totalOrder = _.reduce(
      result[key],
      (sum, item) => sum + item[query.measures[0]],
      0
    )
    const totalAOV = Number(totalMoneyOrder / totalOrder).toFixed(2)
    series.push(totalAOV)
  })
  const options = {
    xaxis: {
      categories,
    },
    tooltip: { x: { show: false }, marker: { show: false } },
  }
  return {
    options,
    series: [
      {
        name: 'orders',
        data: series,
      },
    ],
  }
}
function AverageOrderValueTimeLine(props) {
  const { query } = props
  const classes = useStyles()
  const { resultSet }: any = useCubeQuery(query)
  if (resultSet) {
    const { series, options } = handleDataCube(
      resultSet.loadResponse.data,
      query
    )
    return (
      <div>
        <Grid item>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
            variant="body2"
          >
            Average Order Value Time Line
          </Typography>
        </Grid>
        <ChartLine series={series} options={options} />
      </div>
    )
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '300px',
      }}
    >
      <CircularProgress color="secondary" />
    </div>
  )
}

export default AverageOrderValueTimeLine
