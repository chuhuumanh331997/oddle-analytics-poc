import { useCubeQuery } from '@cubejs-client/react'
import { CircularProgress } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { ChartColumnStacked } from '../../charts'

export default function SalesBySomeThing({
  query,
  type,
}: {
  query: Record<string, unknown>
  type: string
}) {
  const processDataSaleByOrderValue = (dataSet) => {
    const counterArray = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    dataSet.forEach((element) => {
      const orderValue = element['AnalyticsCubePoc.totalAmount']
      const numberOrder = element['AnalyticsCubePoc.count']
      if (orderValue >= 0 && orderValue < 25) counterArray[0] += numberOrder
      else if (orderValue >= 25 && orderValue < 50)
        counterArray[1] += numberOrder
      else if (orderValue >= 50 && orderValue < 75)
        counterArray[2] += numberOrder
      else if (orderValue >= 75 && orderValue < 100)
        counterArray[3] += numberOrder
      else if (orderValue >= 100 && orderValue < 150)
        counterArray[4] += numberOrder
      else if (orderValue >= 150 && orderValue < 200)
        counterArray[5] += numberOrder
      else if (orderValue >= 200 && orderValue < 250)
        counterArray[6] += numberOrder
      else if (orderValue >= 250 && orderValue < 300)
        counterArray[7] += numberOrder
      else if (orderValue >= 300) counterArray[8] += numberOrder
    })
    setChartData(() => [
      {
        name: 'Number of Orders',
        data: counterArray,
      },
    ])
    setXaxis(() => ({
      type: 'category',
      categories: [
        '00. $0-25',
        '01. $25-50',
        '02. $50-75',
        '03. $75-100',
        '04. $100-150',
        '05. $150-200',
        '06. $200-250',
        '07. $250-300',
        '08. More than $300',
      ],
    }))
  }
  const processDataSalebyOrderType = () => {
    setXaxis(() => ({
      type: 'datetime',
      categories: [
        '04/30/2021 GMT',
        '05/01/2021 GMT',
        '05/02/2021 GMT',
        '05/03/2021 GMT',
        '05/04/2021 GMT',
        '05/05/2021 GMT',
        '05/06/2021 GMT',
        '05/07/2021 GMT',
      ],
    }))

    setChartData(() => [
      {
        name: 'Delivery Sales',
        data: [
          5354.51, 12215.3, 10470.38, 1264.31, 712.62, 2963.68, 3524.59,
          6637.63,
        ],
      },
      {
        name: 'Pickup Sales',
        data: [0, 1810.02, 2086.07, 0, 31.03, 0, 286.02, 1472.11],
      },
    ])
  }
  const [dataSet, setdataSet] = useState(null)
  const [chartData, setChartData] = useState([])
  const [xaxis, setXaxis]: any = useState({
    type: 'category',
    categories: [],
  })
  const { resultSet }: any = useCubeQuery(query)
  useEffect(() => {
    const rs: any = resultSet
    if (rs) {
      setdataSet(() => rs?.loadResponse?.data)
    }
  }, [resultSet])
  useEffect(() => {
    if (dataSet) {
      if (type === 'Sales by Order Value') processDataSaleByOrderValue(dataSet)
      else if (type === 'Sales by Order Type') processDataSalebyOrderType()
    }
  }, [dataSet])
  return (
    <div>
      {dataSet ? (
        <ChartColumnStacked series={chartData} xaxis={xaxis} type={type} />
      ) : (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '300px',
          }}
        >
          <CircularProgress color="secondary" />
        </div>
      )}
    </div>
  )
}
