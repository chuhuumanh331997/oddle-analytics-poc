import { useCubeQuery } from '@cubejs-client/react'
import {
  Card,
  CardContent,
  CircularProgress,
  makeStyles,
} from '@material-ui/core'
import React from 'react'
import ChartOnlyNumber from '../../charts/ChartOnlyNumber'
import { nFormatter } from '../../../utils/nFomatter'

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'rgb(0, 82, 73)',
    backgroundColor: 'rgb(200, 250, 205)',
  },
}))

const handleDataCube = (dataSet) => {
  let sum: any = 0
  dataSet.forEach((data) => {
    const count = Number(data['AnalyticsCubePoc.count'])
    const totalAmount = Number(data['AnalyticsCubePoc.totalAmount'])
    sum += count * totalAmount
  })
  const rs: any = nFormatter(sum, 2)
  return `$${rs[0]}${rs[1]}`
}

function TotalSales(props) {
  const { query } = props
  const { resultSet }: any = useCubeQuery(query)

  const classes = useStyles()
  if (resultSet) {
    const numberOrder = handleDataCube(resultSet.loadResponse.data)

    return (
      <div>
        <Card className={classes.root}>
          <CardContent>
            <ChartOnlyNumber numberOrder={numberOrder} title="Total Sales" />
          </CardContent>
        </Card>
      </div>
    )
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '300px',
      }}
    >
      <CircularProgress color="secondary" />
    </div>
  )
}

export default TotalSales
