import { useCubeQuery } from '@cubejs-client/react'
import {
  Card,
  CardContent,
  CircularProgress,
  makeStyles,
} from '@material-ui/core'
import React from 'react'
import ChartOnlyNumber from '../../charts/ChartOnlyNumber'
import { nFormatter } from '../../../utils/nFomatter'

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'rgb(122, 79, 1)',
    backgroundColor: 'rgb(255, 247, 205)',
  },
}))

const handleDataCube = (dataSet) => {
  let numberOrders: any = 0
  dataSet.forEach((data) => {
    const count = Number(data['AnalyticsCubePoc.count'])
    numberOrders += count
  })
  const rs: any = nFormatter(numberOrders, 0)
  return `${rs[0]}${rs[1]}`
}

function TotalOrders(props) {
  const { query } = props
  const { resultSet }: any = useCubeQuery(query)

  const classes = useStyles()
  if (resultSet) {
    const numberOrder = handleDataCube(resultSet.loadResponse.data)

    return (
      <div>
        <Card className={classes.root}>
          <CardContent>
            <ChartOnlyNumber numberOrder={numberOrder} title="Total Orders" />
          </CardContent>
        </Card>
      </div>
    )
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '300px',
      }}
    >
      <CircularProgress color="secondary" />
    </div>
  )
}

export default TotalOrders
