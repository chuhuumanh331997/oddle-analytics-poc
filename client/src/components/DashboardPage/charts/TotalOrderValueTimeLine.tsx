import React from 'react'
import { useCubeQuery } from '@cubejs-client/react'
import {
  CircularProgress,
  Grid,
  Typography,
  makeStyles,
} from '@material-ui/core'
import moment from 'moment'
import _ from 'lodash'
import { ChartLine } from '../../charts'

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: 500,
  },
}))

const handleDataCube = (data, query) => {
  const categories = []
  const series = []
  const result = _.groupBy(data, query.timeDimensions[0].dimension)
  Object.keys(result).forEach((key) => {
    const dateNow = moment(key).format('DD/MM/YYYY')
    categories.push(dateNow)
    const cacularOrder = _.reduce(
      result[key],
      (sum, item) => sum + item[query.measures[0]],
      0
    )
    series.push(cacularOrder)
  })
  const options = {
    xaxis: {
      categories,
    },
    tooltip: { x: { show: false }, marker: { show: false } },
  }
  return {
    options,
    series: [
      {
        name: 'orders',
        data: series,
      },
    ],
  }
}
function TotalOrderValueTimeLine(props) {
  const { query } = props
  const { resultSet }: any = useCubeQuery(query)
  const classes = useStyles()

  if (resultSet) {
    const { series, options } = handleDataCube(
      resultSet.loadResponse.data,
      query
    )
    return (
      <div>
        <Grid item>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
            variant="body2"
          >
            Total Order Value Timeline
          </Typography>
        </Grid>
        <ChartLine series={series} options={options} />
      </div>
    )
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '300px',
      }}
    >
      <CircularProgress color="secondary" />
    </div>
  )
}

export default TotalOrderValueTimeLine
