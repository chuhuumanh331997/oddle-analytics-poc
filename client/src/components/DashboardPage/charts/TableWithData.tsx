import * as React from 'react'
import { DataGrid } from '@material-ui/data-grid'
import { Grid, Typography } from '@material-ui/core'
import _ from 'lodash'
import dataTable from '../data_table.json'

const processData = (data) => {
  const newdata = []
  let id = 0
  data.forEach((ele) => {
    const newone = _.omit(ele, 'appear_in_number_of_orders')
    id += 1
    newone.id = id
    // newone.total_sales_from_items = formatter.format(
    // 	Number(newone.total_sales_from_items)
    // );
    newdata.push(newone)
  })
  return newdata
}
// const formatter = new Intl.NumberFormat('en-US', {
//   style: 'currency',
//   currency: 'USD',
// })
const rawData = [
  {
    id: 1,
    store_name: 'Keng Eng Kee Seafood',
    number_of_orders: 340,
    total_sales: 48864.27007293701,
  },
]
const TopSaleTable = (props) => {
  const { columns, type } = props
  const rows =
    type === 'Top 15 Products by Units Sold' ? processData(dataTable) : rawData
  return (
    <>
      <Grid>
        <Grid item>
          <Typography color="textSecondary" gutterBottom variant="body2">
            {type}
          </Typography>
        </Grid>
      </Grid>
      <div style={{ height: 620, width: '100%' }}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={rows.length <= 9 ? rows.length : 9}
        />
      </div>
    </>
  )
}

export default TopSaleTable
