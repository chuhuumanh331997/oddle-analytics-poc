import React from 'react'
// import './style.css'

function LoadingPage() {
  return (
    <div className="loading-page">
      <div className="loading">
        <p>Please wait</p>
        <span>
          <i />
          <i />
        </span>
      </div>
    </div>
  )
}

export default LoadingPage
