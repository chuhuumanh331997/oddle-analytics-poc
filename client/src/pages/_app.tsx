import React from 'react'
import Head from 'next/head'
import cubejs from '@cubejs-client/core'

import { CubeProvider } from '@cubejs-client/react'
import type { AppProps } from 'next/app'
import ThemeConfig from '../theme'
import './style.css'

// ----------------------------------------------------------------------
const cubejsApi = cubejs('', {
  apiUrl: process.env.BACKEND_URL,
})
const MyApp = ({ Component, pageProps }: AppProps) => (
  <CubeProvider cubejsApi={cubejsApi}>
    <ThemeConfig>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
      </Head>
      <Component {...pageProps} />
    </ThemeConfig>
  </CubeProvider>
)

export default MyApp
