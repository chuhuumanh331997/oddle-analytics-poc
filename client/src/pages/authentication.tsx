import { Button, Checkbox, FormControlLabel, Grid } from '@material-ui/core'
import { useRouter } from 'next/router'
import React from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import jwt from 'jsonwebtoken'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    // width: "25ch",
  },
  authSame: {
    backgroundColor: 'rgb(255, 255, 255)',
  },
  authWraper: {},
  authRight: {
    width: '100%',
    marginLeft: 'auto',
    boxSizing: 'border-box',
    marginRight: 'auto',
    display: 'block',
    paddingLeft: '16px',
    paddingRight: '16px',
    [theme.breakpoints.up('lg')]: {
      maxWidth: '600px',
      paddingLeft: '24px',
      paddingRight: '24px',
    },
  },
  authLeft: {
    [theme.breakpoints.down('lg')]: {
      display: 'none',
    },
  },
  authRightForm: {
    maxWidth: '480px',
    margin: 'auto',
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  authRightFormhOne: {},
  boxWithMessage: {
    backgroundColor: 'rgb(255, 255, 255)',
    color: 'rgb(33, 43, 54)',
    transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    backgroundImage: 'none',
    overflow: 'hidden',
    boxShadow:
      'rgb(145 158 171 / 24%) 0px 0px 2px 0px,\n    rgb(145 158 171 / 24%) 0px 16px 32px -4px',
    borderRadius: '16px',
    position: 'relative',
    zIndex: 0,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    margin: '45px 0px 45px 45px',
  },
  boxContent: {},
  boxContenthOne: {
    margin: '140px 0px',
    marginBottom: '0px',
    fontFamily: "'Be Vietnam', sans-serif",
    fontWeight: 600,
    fontSize: '29px',
    lineHeight: '1.5',
    paddingLeft: '40px',
    paddingRight: '40px',
    textAlign: 'center',
  },
  boxContentImg: {
    width: '90%',
    height: '100%',
    display: 'block',
    marginTop: '0',
    marginBottom: '140px',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  allMarginTopAndBottom: {
    marginTop: '60px',
    marginBottom: '60px',
  },
  allMarginTopAndBottomABit: {
    marginTop: '20px',
    marginBottom: '20px',
  },
  pAllMarginTopAndBottomABit: {
    fontSize: '15px',
    fontFamily: "'Be Vietnam', sans-serif",
    fontWeight: 400,
    color: 'rgb(192, 161, 161)',
  },
  helperWarning: {
    fontSize: '17px',
    margin: 0,
    marginTop: '15px',
  },
}))

export default function AuthenticatePage() {
  const classes = useStyles()
  const router = useRouter()
  const menuIdToken : any = router.query['menu-id-token']
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  })
  const [isChecked, setIsChecked] = React.useState(false)
  const [isError, setIsError] = React.useState(['', false])
  const [readyToRedirect , setReadyToRedirect] = React.useState(false)
  const handleChangeChecked = () => {
    setIsChecked(() => !isChecked)
  }
  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value })
    setIsError(['' , false])
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword })
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }
  const handleVerifyToken = async () => {
    if(values.password.length === 0) {
      setIsError(() => ['Please fill the password' , true])
      return ;
    }
    try {
      const decoded = jwt.verify(menuIdToken , values.password);
      localStorage.setItem("token" ,menuIdToken )
      localStorage.setItem("key" ,  values.password )
      setReadyToRedirect(() => true)
      router.push(`/dashboard?menuId=${decoded.menuId}`)
    } catch(err) {
      setIsError(['Wrong password!' , true])

    }
  }
  return (
    <Grid container className={classes.authWraper}>
      <Grid item lg={4} className={clsx(classes.authSame, classes.authLeft)}>
        <div className={classes.boxWithMessage}>
          <div className={classes.boxContent}>
            <h1 className={classes.boxContenthOne}>
              This link is password protected
            </h1>
            <img
              className={classes.boxContentImg}
              src="https://secure.holistics.io/images/shareable-link-lock.svg"
              alt="react key"
            />
          </div>
        </div>
      </Grid>
      <Grid item lg={8} className={clsx(classes.authSame, classes.authRight)}>
        <div className={classes.authRightForm}>
          <img
            src="https://secure.holistics.io/images/holistics-logo-colored.svg"
            className={classes.allMarginTopAndBottom}
            alt="logo holistics"
          />
          <h3 className={classes.allMarginTopAndBottomABit}>
            Please enter your password to view it.
          </h3>
          <FormControl className={clsx(classes.textField)} variant="outlined" disabled={readyToRedirect}>
            <InputLabel htmlFor="outlined-adornment-password" >
              Password
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={values.showPassword ? 'text' : 'password'}
              value={values.password}
              onChange={handleChange('password')}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
            {isError[1] ? (
              <FormHelperText error className={classes.helperWarning}>
                {isError[0]}
              </FormHelperText>
            ) : (
              <></>
            )}

            <FormControlLabel
              className={classes.allMarginTopAndBottomABit}
              control={
                <Checkbox
                  checked={isChecked}
                  onChange={handleChangeChecked}
                  name="checkedB"
                  color="primary"
                />
              }
              label="Remember me for 7 days"
            />
            <Button
              variant="contained"
              color="primary"
              className={classes.allMarginTopAndBottomABit}
              onClick={() => handleVerifyToken()}
              disabled={readyToRedirect}
            >
              Submit
            </Button>
            <p className={classes.pAllMarginTopAndBottomABit}>
              Don`&apos;`t know the password? Please contact the link owner for
              it.
            </p>
          </FormControl>
        </div>
      </Grid>
    </Grid>
  )
}
