import React from 'react'
import dynamic from 'next/dynamic'
import { makeStyles } from '@material-ui/styles'

const DashboardPage = dynamic(() => import('../components/DashboardPage'), {
  ssr: false,
})

const styles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
}))

const Dashboard = (): JSX.Element => {
  const classes = styles()
  return (
    <div className={classes.root}>
      <DashboardPage />
    </div>
  )
}

export default Dashboard
