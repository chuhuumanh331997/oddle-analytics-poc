import React from 'react'


const Home = (): JSX.Element => (
    <div className="error-page">
        <div className="error-page__content">
          <h1 className="title">Oops! (#422)</h1>
          <p className="description">No shareable link found</p>
        </div>
      </div>
)

export default Home
