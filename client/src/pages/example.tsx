import React from 'react'
import dynamic from 'next/dynamic'
import Container from '@material-ui/core/Container'
import { Divider } from '@material-ui/core'

const ChartBar = dynamic(() => import('../components/charts/ChartBar'), {
  ssr: false,
})
const ChartArea = dynamic(() => import('../components/charts/ChartArea'), {
  ssr: false,
})
const ChartColumnMultiple = dynamic(
  () => import('../components/charts/ChartColumnMultiple'),
  {
    ssr: false,
  }
)
const ChartColumnNegative = dynamic(
  () => import('../components/charts/ChartColumnNegative'),
  {
    ssr: false,
  }
)
const ChartColumnSingle = dynamic(
  () => import('../components/charts/ChartColumnSingle'),
  {
    ssr: false,
  }
)
// const ChartColumnStacked = dynamic(
//   () => import('../components/charts/ChartColumnStacked'),
//   {
//     ssr: false,
//   }
// )
const ChartDonut = dynamic(() => import('../components/charts/ChartDonut'), {
  ssr: false,
})
const ChartLine = dynamic(() => import('../components/charts/ChartLine'), {
  ssr: false,
})
const ChartMixed = dynamic(() => import('../components/charts/ChartMixed'), {
  ssr: false,
})
const ChartPie = dynamic(() => import('../components/charts/ChartPie'), {
  ssr: false,
})
const ChartRadarBar = dynamic(
  () => import('../components/charts/ChartRadarBar'),
  {
    ssr: false,
  }
)
const ChartRadialBar = dynamic(
  () => import('../components/charts/ChartRadialBar'),
  {
    ssr: false,
  }
)
const Example = (): JSX.Element => (
  <Container>
    <h3>ChartBar</h3>
    <ChartBar />
    <Divider />

    <h3>ChartArea</h3>
    <ChartArea />
    <Divider />

    <h3>ChartColumnMultiple</h3>
    <ChartColumnMultiple />
    <Divider />

    <h3>ChartColumnNegative</h3>
    <ChartColumnNegative />
    <Divider />

    <h3>ChartColumnSingle</h3>
    <ChartColumnSingle />
    <Divider />

    {/* <h3>ChartColumnStacked</h3>
    <ChartColumnStacked /> */}
    <Divider />

    <h3>ChartDonut</h3>
    <ChartDonut />
    <Divider />

    <h3>ChartLine</h3>
    <ChartLine />
    <Divider />

    <h3>ChartMixed</h3>
    <ChartMixed />
    <Divider />

    <h3>ChartRadarBar</h3>
    <ChartRadarBar />
    <Divider />

    <h3>ChartRadialBar</h3>
    <ChartRadialBar />
    <Divider />
    <h3>ChartPie</h3>
    <ChartPie />
    <Divider />
  </Container>
)

export default Example
